__author__ = 'David Tadres'
# IDEA:
# Collect the temperature and humidity data save it locally and THEN
# copy it to the remote repository

# IMPORTANT: By necessity each Raspberry Pi needs to have a unique
# ID. This is used on the google drive to distinguish data between
# different stations. If you only have one station offloading data on
# your google drive you don't have to worry about this. If you use
# more than one, however, you MUST change the name, otherwise you'll
# start overwriting data (each RPi overwrites the others data).

# Note: There's a strange bug that happens after ~24 hours. The whole
# GUI just quits.
# First I thought it might be due to a memory leak, for example
# because we have to keep this large list in memory for plotting.
# It seems, however, that it should work as the following code
# snippet will show that python memory allocation should work just fine:

# the_list = [0]
# counter = 0
# runner = 100
#while counter < runner:
#    print(counter)
#    the_list.append(counter)
#    del the_list[0]
#    print(sys.getsizeof(the_list))
#    counter +=1

# This should always return size '64'. If the "del" is removed,
# size increases quickly.

# What else could it be?

# Inspired by https://pimylifeup.com/raspberry-pi-humidity-sensor-dht22/

import datetime
import os
import subprocess
import time
import tkinter as tk
from pathlib import Path
from shutil import copyfile

import matplotlib.backends.backend_tkagg as tkagg
import matplotlib.dates as mdates
from matplotlib.figure import Figure

MEASURE = True # This must be on on the RPi for the actual
# measurements. If this is 'False' we are in a test mode to optimize
# plots etc.

REMOTE = 'NAS terminal'
# Options: 'NAS' if you have mounted a remote drive, e.g. NAS in a folder called /pi/home/windowshare
# 'NAS terminal' # If you have have permission error while using NAS, try this option. It calls sudo before copying
# 'Google Drive' if you are using RClone to write directly to a google drive.

if MEASURE :
    import Adafruit_DHT

    THIS_RPI_ID = 'EnvStation1' # must be different for each RPi!

    DHT_SENSOR = Adafruit_DHT.DHT22
    DHT_PIN = 4 # shouldn't be necessary to change this ever

    original_path = os.getcwd()
    data_path = "data"
    Path(data_path).mkdir(parents=True, exist_ok=True)

else:
    fake_temp = [20, 24, 20, 24, 20, 24, 20, 24, 20, 24, 20, 24, 20, 24]
    fake_hum = [10, 80, 10, 80, 10, 80, 10, 80, 10, 80, 10, 80, 10, 80]

y_lim = (15,25)
sleep_time = 5 # 5 seconds between measurements
keep_data = 24 # hours - how much data to keep for display

class Window(tk.Frame):

    def __init__(self, master=None):
        """
        Create the window and initialize the necessary variables
        """
        tk.Frame.__init__(self, master)
        self.master = master

        self.master.wm_title('DHT22 Temperature/Humidity Graph')

        # Used once, then thrown away
        self.first_measurement = True

        # other variables
        self.display_time = None
        self.future_time = None

        if not MEASURE:
            self.offline_counter = 0

        self.frame = tk.Frame(self.master)
        self.frame.grid(row=0, column=0)

        # how many datapoints to keep
        self.keep_datapoints = int(60 / sleep_time * 60 * keep_data)
        self.datapoints_temp = [0] * self.keep_datapoints  # create
        # list with correct length but filled with zeros
        self.datapoints_hum = [0] * self.keep_datapoints
        # make up time data for proper plotting
        self.datetime_for_plotting = [datetime.datetime.now() +
                            datetime.timedelta(seconds=-i)
                                      for i in range(len(
                self.datapoints_temp*sleep_time))]
        self.datetime_for_plotting = self.datetime_for_plotting[::sleep_time]

        ###########################################################
        # LEFT PART: LABELS
        ###########################################################
        # TEMPERATURE LABEL
        self.current_temp_label = tk.Label(self.frame,
                                     text='Current Temperature')
        self.current_temp_label.grid(row=0, column=0)
        self.current_temp_label.config(font=("Arial", 10))

        # Create a textvariable to bind to the label - makes it trivial
        # to update afterwards
        self.temperature_label_var = tk.StringVar()
        self.temperature_label_var.set('20C')
        self.current_temp_value = tk.Label(
            self.frame,
            textvariable = self.temperature_label_var)
        self.current_temp_value.grid(row=1,column=0)
        self.current_temp_value.config(font=("Arial", 15))

        # HUMIDITY LABEL
        # Create a textvariable to bind to the label - makes it trivial
        # to update afterwards
        self.humidity_label_var = tk.StringVar()
        self.humidity_label_var.set('80%')
        self.current_hum_label = tk.Label(self.frame,
                                          text='Current Humidity')
        self.current_hum_label.grid(row=2, column=0)
        self.current_hum_label.config(font=("Arial", 10))

        self.current_hum_value = tk.Label(
            self.frame,
            textvariable=self.humidity_label_var)
        self.current_hum_value.grid(row=3,column=0)
        self.current_hum_value.config(font=("Arial", 15))

        ############################################################
        # RIGHT PART: PLOTS
        ############################################################
        # TEMPERATURE PLOT
        self.fig_temp = Figure(figsize=(3,2))
        self.ax_temp = self.fig_temp.add_subplot(111)
        self.ax_temp_object, = self.ax_temp.plot(
            self.datetime_for_plotting,
            self.datapoints_temp
        )
        # Format x axis so that times are properly formatted
        myFmt = mdates.DateFormatter('%H:%M')
        self.ax_temp.xaxis.set_major_formatter(myFmt)
        # As we have too many x ticks, we subsample and only display 3
        #xticks = self.ax_temp.get_xticks()
        #self.ax_temp.set_xticks(xticks[::4])
        self.ax_temp.set_ylim(y_lim)
        self.ax_temp.set_ylabel('Temperature')
        self.fig_temp.autofmt_xdate()
        self.fig_temp.tight_layout()

        # bind the plot to the GUI
        self.canvas_temp = tkagg.FigureCanvasTkAgg(
            self.fig_temp, master=self.frame)
        self.canvas_temp.draw()
        self.canvas_temp.get_tk_widget().grid(row=0, column=2,
                                              rowspan=4,
                                              columnspan=3)
        self.canvas_temp_background = \
            self.canvas_temp.copy_from_bbox(self.ax_temp.bbox)

        ##############################################################
        # BOTTOM ROW
        ##############################################################
        self.display_label_left = tk.Label(self.frame,
                                      text='Display last')
        self.display_label_left.grid(row=5, column=2, sticky="E")

        # Menu giving the option to display different past timepoints
        self.display_time_var = tk.DoubleVar()
        self.display_time_available = (0.5,
                                       1,
                                       2,
                                       6,
                                       12,
                                       24)
        self.display_time_var.set(self.display_time_available[5])

        self.display_time_menu = tk.OptionMenu(
            self.frame,
            self.display_time_var,
            *self.display_time_available,
        )
        self.display_time_menu.grid(row=5, column=3)

        self.display_label_right = tk.Label(self.frame,
                                      text='hours')
        self.display_label_right.grid(row=5, column=4, sticky = "W")

        # Button to allow user to exit without crashing the program
        quit_button = tk.Button(self.frame, text="Quit", command=root.destroy)
        quit_button.grid(row=5, column=0, sticky="W")

        # call the collect_data function
        self.collect_data()

    def collect_data(self):
        """
        This function repeats periodically as defined in 'sleep_time'.

        It collects the temperature and humidity data from the DHT
        sensor and saves the data locally and then copies the file to
        the Remote (e.g. NAS or Google Drive).

        Then, it updates the tkinter window updating the current
        temperature and humidity and also updating the plot with the
        newest temperature data.
        """

        # call this function periodically as often as defined in
        # 'sleep time'
        self.master.after(int(sleep_time*1000), self.collect_data)

        ##############################################################
        # COLLECT THE DATA AND SAVE IT
        ##############################################################
        if MEASURE:
            os.chdir(Path(original_path, data_path))

            today = datetime.date.today()  # check date
            current_year = time.strftime('%Y')
            current_month = time.strftime('%m')

            complete_path = Path(original_path, data_path, current_year,
                                 current_month)

            Path(complete_path).mkdir(parents=True, exist_ok=True)
            os.chdir(complete_path)

            # collect temperature and humidity data
            # had to cut down on the retries as it was ballooning the
            # time between measurements. Found the function here:
            # https://github.com/adafruit/Adafruit_Python_DHT/blob/master/Adafruit_DHT/common.py
            # With this we should more often get a "None" but the timing
            # should be ok
            humidity, temperature = Adafruit_DHT.read_retry(
                sensor=DHT_SENSOR,
                pin=DHT_PIN,
                retries=3,
                delay_seconds=1)
        # next, check if the first datapoint needs to be deleted
        if len(self.datapoints_temp) >= self.keep_datapoints:
            del self.datapoints_temp[0]
            del self.datapoints_hum[0]
            del self.datetime_for_plotting[0]

        if MEASURE:
            # append the datapoint to the list
            try:
                current_temp = round(temperature, 2)
                # sometimes there are funny and unrealistic read errors
                # where the temperature is 10C lowers than in the
                # previous measurement. This can not really happen so
                # correct for this to keep plot tidy
                try:
                    if abs(self.datapoints_temp[-1] - current_temp) < 5 or \
                            self.first_measurement:
                        self.datapoints_temp.append(current_temp)
                    else:
                        self.datapoints_temp.append(self.datapoints_temp[-1])
                except TypeError:
                    # This happens if sensor returned error in the
                    # PREVIOUS cycle
                    self.datapoints_temp.append(current_temp)

            except TypeError: # This happens if sensor returns 'None'
                self.datapoints_temp.append(temperature)
            try:
                # and same for the humidity sensor!
                current_hum = round(humidity, 2)
                try:
                    if abs(self.datapoints_hum[-1] - current_hum) < 10 or \
                            self.first_measurement:
                        self.datapoints_hum.append(current_hum)
                    else:
                        self.datapoints_hum.append(self.datapoints_hum[-1])
                except TypeError:
                    # This happens if sensor returned error in the
                    # PREVIOUS cycle
                    self.datapoints_hum.append(current_hum)
            except TypeError:
                self.datapoints_hum.append(humidity)
            # Turn self.first_measurement off forever
            self.first_measurement = False
        else:
            self.datapoints_temp.append(fake_temp[self.offline_counter])
            self.datapoints_hum.append(fake_hum[self.offline_counter])
            self.offline_counter += 1

        current_time = time.strftime('%H:%M:%S')

        if MEASURE:
            # filename
            current_filename = str(today) + '_temp_hum.csv'

            if os.path.isfile(current_filename):
                with open(current_filename, 'a+') as file:
                    if temperature is not None and humidity is not None:
                        file.write('{0},{1},{2:0.1f},{3:0.1f}\r\n'.format(
                            str(today), current_time,
                            temperature, humidity))
                    else:
                        file.write('{0},{1},{2},{3}\r\n'.format(
                            str(today), current_time,
                            'NA', 'NA'))
            else:
                with open(current_filename, 'w+') as file:
                    if temperature is not None and humidity is not None:
                        file.write('{0},{1},{2:0.1f},{3:0.1f}\r\n'.format(
                            str(today), current_time,
                            temperature, humidity))
                    else:
                        file.write('{0},{1},{2},{3}\r\n'.format(
                            str(today), current_time,
                            'NA', 'NA'))

            path_filename = Path(complete_path, current_filename)

            if REMOTE == 'NAS':
                # Define the remote path. Idea is that we keep one folder for each month and different folders for
                # years. MUST BE TESTED FIRST!
                remote_path = Path('/home/pi/windowshare/' , THIS_RPI_ID, current_year, current_month)
                copyfile(path_filename, Path(remote_path, path_filename.name)) #TEST THIS!

            elif REMOTE == 'NAS terminal':
                remote_path = "/home/pi/windowshare/" + THIS_RPI_ID + "/" + current_year + "/" + current_month +"/"
                # rsync -a myfile /foo/bar/
                #subprocess.run(["sudo", "mkdir", "-p", remote_path, "&&", "cp", path_filename, remote_path])
                subprocess.run(["sudo", "rsync", "-a", path_filename, remote_path])

            elif REMOTE == 'Google Drive':
                remote_path = "remote:" + THIS_RPI_ID + "/" + current_year + '/' + \
                              current_month

                # update the current file using rclone
                subprocess.run(["rclone", "copy", path_filename,
                                remote_path])
        # Testing only
        #print('current temp reading: ' + repr(temperature))

        ###############################################################
        # PLOT THE DATA
        ###############################################################
        # Update the temperature/humidity variable
        self.temperature_label_var.set(repr(self.datapoints_temp[-1])+'C')
        self.humidity_label_var.set(repr(self.datapoints_hum[-1])+'%')

        # append datetime to list
        self.datetime_for_plotting.append(datetime.datetime.now())

        self.ax_temp_object.set_data(
            self.datetime_for_plotting,self.datapoints_temp)

        if self.display_time != self.display_time_var.get() or \
                self.display_time is None:
            self.display_time = self.display_time_var.get()
            if self.display_time == 0.5:
                self.future = 3
            elif self.display_time == 1.0:
                self.future = 5
            elif self.display_time == 2.0:
                self.future = 7
            elif self.display_time == 6.0:
                self.future = 10
            elif self.display_time == 12.0:
                self.future = 20
            elif self.display_time == 24.0:
                self.future = 30

        current_time = datetime.datetime.now() + datetime.timedelta(
            minutes=self.future) # Probably good to change this value in case
        # we change display time
        self.ax_temp.set_xlim(
            current_time-datetime.timedelta(hours=self.display_time),
            current_time)

        # UPDATE THE FRAME - Must be last line in this function!
        # blitting as draw would take too long
        self.canvas_temp.restore_region(self.canvas_temp_background)
        self.ax_temp.draw_artist(self.ax_temp_object)

        # Use the below if only the inside of the axis (i.e. not the
        # xticks) need to be updated
        #self.canvas_temp.blit(self.ax_temp.bbox)

        # Use the below if more than just the inside of the axis
        # needs to be updated. In this case it is necessary to update
        # the x_lim
        self.fig_temp.canvas.draw()
        self.frame.update()

root = tk.Tk()
app = Window(root)
root.mainloop()