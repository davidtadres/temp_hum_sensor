# When to start plotting
# Must use the following format:
# YEAR-MO-DA-HO:MI:SE
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np

import my_functions

start_time = '2020-07-21-23:00:00' # Must use exactly this format!
end_time = -1 # -1 means until now, also possible to give precise
# time coordinates with identical format as in start_time
env_data = my_functions.collect_data(
    start_time,
    end_time,
    path_to_data='//louis-nas1.mcdb.ucsb.edu/temperature_humidity'
                 '/EnvStation2',
    median_filter = 5)

individual_dates = env_data['Date'].unique()

# Drop all the rows containing NaNs
#env_data = env_data.dropna()

fig = plt.figure()
ax_temp = fig.add_subplot(211)
ax_temp.xaxis.set_major_locator(ticker.MultipleLocator(2))

ax_hum = fig.add_subplot(212, sharex = ax_temp)

for date in individual_dates:
    #if date == '2020-07-25':
    #    break

    print(date)
    # have a small problem with x values - as we are strongly
    # oversampling we can just assume that we have datapoints every 5
    # second (NOT true as some values are NaNs - but probably ok for
    # plotting to see trend) and just plot these as x values

    x_values = np.arange(
        0, 24, 24/(env_data.loc[env_data['Date'] == date].shape[0]))

    ax_temp.plot(
            x_values,
            env_data.loc[env_data['Date'] == date]['Temp_filtered'],
            label=date)

    ax_hum.plot(
            x_values,
            env_data.loc[env_data['Date'] == date]['Humididty_filtered']
    )

ax_temp.legend()



###
