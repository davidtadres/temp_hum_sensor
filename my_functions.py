import time
from pathlib import Path

import pandas as pd


# path_to_data = Path('R:/RPi_Holger') # Rclone folder
#path_to_data = Path('//louis-nas1.mcdb.ucsb.edu/temperature_humidity'
#                    '/EnvStation1')

def collect_data(
        start_time,
        end_time,
        path_to_data = Path(
            '//louis-nas1.mcdb.ucsb.edu/temperature_humidity/EnvStation1'),
        median_filter = 3
        ):
    """
    This function grabs temperature and humidity data as requested
    between start_time and end_time


    :param start_time: Starting time, format: YEAR-MO-DA-HO:MI:SE
    :param end_time: End time, format: YEAR-MO-DA-HO:MI:SE, -1 is now
    :param path_to_data: the path where the data is located
    :param median_filter: length of the median filter applied to the
    sensor measurements. There are measurement errors. Increase if
    many errors remain.
    :return: pandas array containing all the collected data and
    filtered data.
    """

    st_year = start_time.split('-')[0]
    st_month = start_time.split('-')[1]
    st_day = start_time.split('-')[2]
    st_hour = start_time.split('-')[3].split(':')[0]
    st_minutes = start_time.split(':')[1]
    st_seconds = start_time.split(':')[2]

    # grab current time if endtime is not defined
    if end_time == -1:
        end_year = time.strftime('%Y')
        end_month = time.strftime('%m')
        end_day = time.strftime('%d')
        end_hour = time.strftime('%H')
        end_minutes = time.strftime('%M')
        end_seconds = time.strftime('%S')
    else:
        # if endtime is defined, take that time
        end_year = start_time.split('-')[0]
        end_month = start_time.split('-')[1]
        end_day = start_time.split('-')[2]
        end_hour = start_time.split('-')[3].split(':')[0]
        end_minutes = start_time.split(':')[1]
        end_seconds = start_time.split(':')[2]

    #temperature = []
    #humidity = []
    #date_data = []
    #time_data =[]

    all_data = None

    collecting_data_year = True

    year = int(st_year)
    month = int(st_month)
    day = int(st_day)

    first_year = True

    # months with 31 days:
    long_months = [1,3,5,7,8,10,12]
    # months with 30 days
    short_month = [4,6,9,11]

    while collecting_data_year:
        # If going from one year to the next, need to reset the month
        if first_year:
            first_year = False
        else:
            month = 1

        path_year = Path(path_to_data, str(year))
        print(path_year)

        collecting_data_month = True

        while collecting_data_month:
            if month < 10:
                month_string = '0' + str(month)
            else:
                month_string = str(month)

            path_year_month = Path(path_year, month_string)
            print(path_year_month)

            collecting_data_days = True

            while collecting_data_days:
                print(day)

                # Days are always 2 digits, so if below 10, has a 0
                # before the actual number.
                if day < 10:
                    day_string = '0' + str(day)
                else:
                    day_string = str(day)

                filename = str(year) + '-' + \
                           month_string + '-' + \
                           day_string + '_temp_hum.csv'
                path_csv_file = Path(path_year_month,filename)

                # is today the last day we are looking at
                # the data?
                if int(end_year) == year:
                    #print('end_year true')
                    if int(end_month) == month:
                        #print('end month true')
                        if int(end_day) == day:
                            #print('end_day true')
                            collecting_data_days = False
                try:
                    data = pd.read_csv(path_csv_file,
                            header=None,
                            names=["Date", "Time", "Temp", "Hum"],
                                       engine='python')
                    if all_data is None:
                        all_data = data
                    else:
                        all_data = all_data.append(data)
                except OSError as e:
                    print(e)

                # True except on last day!
                #if collecting_data_days:
                #temperature.append(data['Temp'])
                #humidity.append(data['Hum'])
                #date_data.append(data['Date'])
                #time_data.append(data['Time'])

                # leap year rules (wikipedia):
                # if divisible by 4 is a leap year (29 days) else not (28)
                # days.
                # If divisible by 100 we skip the leap year.
                # unless divisible by 400, then we do not skip the leap year
                if month == 2:
                    # if not divisble by 4, is a not a lap year
                    if year%4 != 0:
                        if day == 28:
                            #collecting_data_days = False
                            month += 1
                        else:
                            day += 1
                    # if divisible by 100, is not a lap year
                    elif year%100 == 0:
                        if day == 28:
                            #collecting_data_days = False
                            month += 1
                        else:
                            day += 1
                    # THIS WILL BREAK IN THE YEAR 2400!
                    elif day == 29:
                            #collecting_data_days = False
                            month += 1
                    else:
                        day += 1

                elif any(month==n for n in short_month):
                    if day == 30:
                        collecting_data_days = False
                        day = 1
                    else:
                        day += 1
                # If the current month has 31 days:
                elif any(month==n for n in long_months):
                    if day == 31:
                        collecting_data_days = False
                        day = 1
                    else:
                        day += 1

            # is this a special year, meaning is it last
            # year we are looking at the data?
            if int(end_year) == year:
                # if this is the last year and the current month is the
                # last month, turn the while loop off
                if int(end_month) == month:
                    collecting_data_month = False
            # if it's the end of the year turn the month while loop off
            if month == 12:
                collecting_data_month = False
            # Else keep adding a month
            else:
                month +=1

        if int(end_year) == year:
            collecting_data_year = False
        else:
            year += 1


    # as we have combined a number of different dataframes, the index is off
    all_data = all_data.reset_index(drop=True)
    # Now we need to fix some data. E.g. it's unreasonable to believe that
    # there are huge dips (>3C) in 5 seconds. Generally, we could
    # just use a boxcar filter that runs with median filtering!
    all_data['Temp_filtered'] = all_data['Temp'].rolling(median_filter).median()
    all_data['Humididty_filtered'] = all_data['Hum'].rolling(median_filter).median()
    all_data['datetime'] = all_data['Date'] + '-' + data['Time']

    return(all_data)
