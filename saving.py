# strongly inspired by https://pimylifeup.com/raspberry-pi-humidity-sensor-dht22/
import datetime
import os
import subprocess
import time
from pathlib import Path

import Adafruit_DHT

THIS_RPI_ID = 'RPi_Holger'

DHT_SENSOR = Adafruit_DHT.DHT22
DHT_PIN = 4 # Make sure that's correct
sleep_time = 5 # 5 seconds between measurements

original_path = os.getcwd()
data_path = "data"

Path(data_path).mkdir(parents=True, exist_ok=True)

while True: # run until infinity
    os.chdir(Path(original_path, data_path))
    start_time = time.time()

    today = datetime.date.today()  # check date
    current_year = time.strftime('%Y')
    current_month = time.strftime('%m')
    #current_day = time.strftime('%d')

    complete_path = Path(original_path, data_path, current_year, current_month)

    Path(complete_path).mkdir(parents=True, exist_ok=True)
    os.chdir(complete_path)

    humidity, temperature = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)
    current_time = time.strftime('%H:%M:%S')

    # filename
    current_filename = str(today) + '_temp_hum.csv'

    if os.path.isfile(current_filename):
        with open(current_filename, 'a+') as file:
            if temperature is not None and humidity is not None:
                file.write('{0},{1},{2:0.1f},{3:0.1f}\r\n'.format(
                    str(today), current_time,
                    temperature, humidity))
            else:
                file.write('{0},{1},{2},{3}\r\n'.format(
                    str(today), current_time,
                    'NA', 'NA'))
    else:
        with open(current_filename, 'w+') as file:
            if temperature is not None and humidity is not None:
                file.write('{0},{1},{2:0.1f},{3:0.1f}\r\n'.format(
                    str(today), current_time,
                    temperature, humidity))
            else:
                file.write('{0},{1},{2},{3}\r\n'.format(
                    str(today), current_time,
                    'NA', 'NA'))

    path_filename = Path(complete_path , current_filename)

    remote_path = "remote:" + THIS_RPI_ID + "/" + current_year + '/' + \
              current_month

    # update the current file using rclone
    subprocess.run(["rclone", "copy", path_filename,
                    remote_path])

    # wait for the next instance
    time_used = time.time() - start_time
    if sleep_time > time_used:
        time.sleep(sleep_time-time_used)
