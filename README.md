Detailed description on how to build this will come eventually.

If you need this, please open a ticket. I will try to prioritize updating
this repository.

# Get the monitor to work:

https://www.waveshare.com/wiki/3.5inch_RPi_LCD_(A)

###In summary:
Connect both the LCD and a monitor using HDMI, a mouse and keyboard and start the system.

type `sudo raspi-config` in the terminal. 

Go to 'boot options' and select 'Desktop Autologin'

Go to terminal and type the following
 
 `git clone https://github.com/waveshare/LCD-show.git` 
 
`cd LCD-show`

`chmod +x LCD35-show`

`./LCD35-show`

After reboot, the monitor works.

# Connect to NAS

https://www.raspberrypi.org/documentation/remote-access/samba.md

###In summary:

`sudo apt update`

`sudo apt install samba samba-common-bin smbclient cifs-utils`

`mkdir windowshare`

`sudo mount.cifs //<hostname or IP address>/share /home/pi/windowshare -o user=<name>,pass=<password>`

In our lab, the hostname would be: `louis-nas1.mcdb.ucsb.edu`

Ask David or John Echeveste for username and password

# run the script

First, clone the repository

`git clone https://gitlab.com/davidtadres/temp_hum_sensor`

In the terminal type

`python3 start_sensor.py`
# If using google drive as remote

On the RPi

`sudo pip3 install Adafruit_DHT`

`sudo apt-get install rclone`

Then do the rclone config in order to mount the remote drive. 
Important: The remote must have the name "remote:"

Now is also a good time to change the Name of the RPi in the file
'savn_display.py' in line 26 (variable: THIS_RPI_ID). IF you don't
do this you might start overwritting your data on the remote.

# before going on the internet, secure the RPi
https://www.raspberrypi.org/documentation/configuration/security.md

essentially:
1) change password
2) change sudo rules

# Notes
IP of NAS is:  128.111.209.150
If it changes, on Windows learn IP by using command prompt and typing ping louis-nas1.mcdb.ucsb.edu

EnvStation1: MAC-address: b8:27:eb:00:04:12
EnvStation2: MAC-address: b8:27:eb:4e:85:db

# Turn screen blanking off

Raspbian turns the screen automatically off after a while. This webpage shows you how
to disable that behavior to always see the temperature.

Go to preferences-> Raspberry Pi Configuration, go to 'Display' tab, hit 'Disable' and hit TAB twice (as you can't see
the OK button on the 3.5inch LCD). Then enter and reboot